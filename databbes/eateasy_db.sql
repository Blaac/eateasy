-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Creato il: Gen 02, 2018 alle 19:12
-- Versione del server: 5.6.35
-- Versione PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eateasy_db`
--
CREATE DATABASE IF NOT EXISTS `eateasy_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `eateasy_db`;

-- --------------------------------------------------------

--
-- Struttura della tabella `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `Nome` varchar(30) NOT NULL,
  `Descrizione` varchar(200) NOT NULL,
  PRIMARY KEY (`Nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categoria`
--

INSERT INTO `categoria` (`Nome`, `Descrizione`) VALUES
('Calzoni', 'I calzoni più grandi di sempre'),
('Crescioni', 'Crescioni romagnoli dallo spessore tipico cesenate'),
('Panini', 'Panini farciti'),
('Pasta', 'Le paste tipiche della tradizione romagnola'),
('Piadine', 'Piadine romagnole dallo spessore tipico cesenate'),
('Pizze', 'Pizze in puro stile napoletano');

-- --------------------------------------------------------

--
-- Struttura della tabella `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `Mail` varchar(30) NOT NULL,
  `Nome` varchar(30) NOT NULL,
  `Cognome` varchar(30) NOT NULL,
  `Indirizzo` varchar(200) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `Admin` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Mail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `cliente`
--

INSERT INTO `cliente` (`Mail`, `Nome`, `Cognome`, `Indirizzo`, `Password`, `Admin`) VALUES
('admin@admin.com', 'Luca', 'Giulianini', 'via Sacchi 9', '$2y$10$ZB652jPiLw1kNB1DSvb8oOMMmdVNPFjX7XOSyV0fXx5E6WBeGTEI2', b'1'),
('pinco@pallo.com', 'Pinco', 'Pallo', 'Via Sacchi, 8, 47521 Cesena FC, Italia', '$2y$10$zP8/utOTJuOg1hhmnwm.Y.CTBK8/u10fs0QOBq4mTGmutfGfCf9EC', b'0');

-- --------------------------------------------------------

--
-- Struttura della tabella `dettaglioordine`
--

DROP TABLE IF EXISTS `dettaglioordine`;
CREATE TABLE IF NOT EXISTS `dettaglioordine` (
  `Ordine` int(11) NOT NULL,
  `Prodotto` int(11) NOT NULL,
  `Quantita` int(11) NOT NULL,
  `Prezzo` float NOT NULL,
  `Sconto` float DEFAULT NULL,
  `Tot` float NOT NULL,
  PRIMARY KEY (`Ordine`,`Prodotto`),
  KEY `Prodotto` (`Prodotto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `dettaglioordine`
--

INSERT INTO `dettaglioordine` (`Ordine`, `Prodotto`, `Quantita`, `Prezzo`, `Sconto`, `Tot`) VALUES
(15, 13, 1, 7, 0, 7),
(16, 14, 1, 6, 0, 6);

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `mail` varchar(50) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `login_attempts`
--

INSERT INTO `login_attempts` (`mail`, `time`) VALUES
('nico.1565@libero.it', '1514679428'),
('nico.1565@libero.it', '1514679553');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

DROP TABLE IF EXISTS `ordine`;
CREATE TABLE IF NOT EXISTS `ordine` (
  `Numero` int(11) NOT NULL AUTO_INCREMENT,
  `Data` date NOT NULL,
  `Cliente` varchar(50) NOT NULL,
  `Indirizzo` varchar(300) NOT NULL,
  `Spedito` bit(1) NOT NULL DEFAULT b'0',
  `Notified` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Numero`),
  KEY `Cliente` (`Cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`Numero`, `Data`, `Cliente`, `Indirizzo`, `Spedito`, `Notified`) VALUES
(15, '2018-01-02', 'pinco@pallo.com', 'Via Sacchi, 8, 47521 Cesena FC, Italia', b'1', b'1'),
(16, '2018-01-02', 'pinco@pallo.com', 'Via Sacchi, 8, 47521 Cesena FC, Italia', b'1', b'1');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

DROP TABLE IF EXISTS `prodotto`;
CREATE TABLE IF NOT EXISTS `prodotto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(500) NOT NULL,
  `Descrizione` varchar(2000) NOT NULL,
  `Immagine` varchar(500) NOT NULL,
  `Prezzo` float NOT NULL,
  `Categoria` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Categoria` (`Categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`Id`, `Nome`, `Descrizione`, `Immagine`, `Prezzo`, `Categoria`) VALUES
(9, 'Pizza Margherita', 'Pizza pomodoro mozzarella', 'images/cibo/margherita.png', 3, 'Pizze'),
(10, 'Pizza al Cotto', 'Pizza pomodoro mozzarella cotto', 'images/cibo/cotto.png', 5, 'Pizze'),
(11, 'Pizza Quattro Formaggi', 'Pizza pomodoro mozzarella taleggio grana gorgonzola', 'images/cibo/quattro_formaggi.png', 6, 'Pizze'),
(12, 'Calzone alle Erbe ', 'Calzone alle erbe semplice', 'images/cibo/calzone.png', 4, 'Calzoni'),
(13, 'Spaghetti alla Carbonara', 'Semplici spaghetti alla carbonara.', 'images/cibo/carbonara.png', 7, 'Pasta'),
(14, 'Lasagne al Ragu', 'Lasagne bolognesi', 'images/cibo/lasagne_ragu.png', 6, 'Pasta'),
(15, 'Strozzapreti ai Sapori di Mare', 'Strozzapreti con gamberi acciughe mazzancolle', 'images/cibo/strozzapreti.png', 8, 'Pasta');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodottomenu`
--

DROP TABLE IF EXISTS `prodottomenu`;
CREATE TABLE IF NOT EXISTS `prodottomenu` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(500) NOT NULL,
  `Descrizione` varchar(2000) NOT NULL,
  `Immagine` varchar(500) NOT NULL,
  `Prezzo` float NOT NULL,
  `Categoria` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `CategoriaMenu` (`Categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodottomenu`
--

INSERT INTO `prodottomenu` (`Id`, `Nome`, `Descrizione`, `Immagine`, `Prezzo`, `Categoria`) VALUES
(9, 'Pizza Margherita', 'Pizza pomodoro mozzarella', 'images/cibo/margherita.png', 3, 'Pizze'),
(10, 'Pizza al Cotto', 'Pizza pomodoro mozzarella cotto', 'images/cibo/cotto.png', 5, 'Pizze'),
(11, 'Pizza Quattro Formaggi', 'Pizza pomodoro mozzarella taleggio grana gorgonzola', 'images/cibo/quattro_formaggi.png', 6, 'Pizze'),
(12, 'Calzone alle Erbe ', 'Calzone alle erbe semplice', 'images/cibo/calzone.png', 4, 'Calzoni'),
(13, 'Spaghetti alla Carbonara', 'Semplici spaghetti alla carbonara.', 'images/cibo/carbonara.png', 7, 'Pasta'),
(14, 'Lasagne al Ragu', 'Lasagne bolognesi', 'images/cibo/lasagne_ragu.png', 6, 'Pasta'),
(15, 'Strozzapreti ai Sapori di Mare', 'Strozzapreti con gamberi acciughe mazzancolle', 'images/cibo/strozzapreti.png', 8, 'Pasta');

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `dettaglioordine`
--
ALTER TABLE `dettaglioordine`
  ADD CONSTRAINT `Ordine` FOREIGN KEY (`Ordine`) REFERENCES `ordine` (`Numero`),
  ADD CONSTRAINT `Prodotto` FOREIGN KEY (`Prodotto`) REFERENCES `prodotto` (`Id`);

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `Cliente` FOREIGN KEY (`Cliente`) REFERENCES `cliente` (`Mail`);

--
-- Limiti per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  ADD CONSTRAINT `Categoria` FOREIGN KEY (`Categoria`) REFERENCES `categoria` (`Nome`);

--
-- Limiti per la tabella `prodottomenu`
--
ALTER TABLE `prodottomenu`
  ADD CONSTRAINT `CategoriaMenu` FOREIGN KEY (`Categoria`) REFERENCES `categoria` (`Nome`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
