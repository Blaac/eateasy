<?php
include 'dbConnectAdmin.php';
include 'secure.php';

sec_session_start();
if(login_check($conn) == true) {
    if (!checkAdmin()) {
        header('Location: ./indexLogged.php');
    }

    $sql = 'SELECT C.Mail, O.Numero, O.Indirizzo, O.Data, O.Spedito FROM cliente AS C, dettaglioordine AS D, ordine AS O WHERE C.Mail = O.Cliente AND O.Numero = D.Ordine  GROUP BY O.Numero ORDER BY O.Spedito';

    if(mysqli_connect_errno()){
        die("conn failed: "
            . mysqli_connect_error()
            . " (" . mysqli_connect_errno()
            . ")");
    }

    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($nome, $numero, $indirizzo, $data, $spedito); // risultato ottenuto.


    while ($stmt->fetch()) {
        if ($spedito == 0) {
        $pieces = explode(",", $indirizzo);
        $pieces = explode(" ", $pieces[0]);
        $indi = $pieces[0] . " " . $pieces[1];
            $myArr = array('TOSEND', $nome, $numero, $indi);
            $myJSON = json_encode($myArr);
            echo $myJSON;
            break;
        }
    }
    $stmt->free_result();

    $stmt->close();
} else {
   echo 'You are not authorized to access this page, please login. <br/>';
     header('Location: index.php');
 }

?>