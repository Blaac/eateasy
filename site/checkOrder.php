<?php
include 'dbConnectAdmin.php';
include 'secure.php';

sec_session_start();
if(login_check($conn) == true) {

        $sql = 'SELECT O.Numero, C.Mail,  O.Data, O.Indirizzo, O.Spedito FROM cliente AS C, ordine AS O WHERE C.Mail = ? AND C.Mail = O.Cliente AND O.Spedito = 1 AND O.Notified = 0 GROUP BY O.Numero ORDER BY  O.Data DESC LIMIT 1';

        if(mysqli_connect_errno()){
            die("conn failed: "
                . mysqli_connect_error()
                . " (" . mysqli_connect_errno()
                . ")");
        }

        $stmt = $conn->prepare($sql);
        $stmt->bind_param('s', $_SESSION['username']);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($numero, $mail, $data, $indirizzo, $spedito); // risultato ottenuto.

        if ($stmt->num_rows == 1) {
            while ($stmt->fetch()) {
            }
            $pieces = explode(",", $indirizzo);
            $pieces = explode(" ", $pieces[0]);
            $indi = $pieces[0] . " " . $pieces[1];
            $myArr = array('SENT', $numero, $mail, $indi);
            $myJSON = json_encode($myArr);
            echo $myJSON;
        }

        $stmt->free_result();

        $stmt->close();

        $conn = new mysqli(HOST, USER, PASSWORD, DATABASE);

        $sql = "UPDATE ordine SET notified = b'1' WHERE ordine.Numero = ?";

        if(mysqli_connect_errno()){
            die("conn failed: "
                . mysqli_connect_error()
                . " (" . mysqli_connect_errno()
                . ")");
        }

        $stmt = $conn->prepare($sql);
        $stmt->bind_param('i', $numero);
        $stmt->execute();
        $stmt->free_result();
        $stmt->close();
            

} else {
   echo 'You are not authorized to access this page, please login. <br/>';
     header('Location: index.php');
 }

?>