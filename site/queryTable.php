<?php
include 'secure.php';
include 'dbConnect.php';


sec_session_start();
if(login_check($conn) == true) {
	$mail =  $_SESSION['username'];


	$sql = 'SELECT P.Nome, P.categoria, D.quantita, P.prezzo FROM prodotto as P, cliente AS C, dettaglioordine AS D, ordine AS O WHERE C.Mail = O.Cliente AND O.Numero = D.Ordine AND C.Mail = ? AND P.Id = D.Prodotto AND O.Numero = ?';

	if (isset($_GET['ordine'])) {

		if(mysqli_connect_errno()){
		    die("conn failed: "
		        . mysqli_connect_error()
		        . " (" . mysqli_connect_errno()
		        . ")");
		}


		$stmt = $conn->prepare($sql);
		$stmt->bind_param('si',$mail,$_GET['ordine']);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($nome, $categoria, $quantita, $prezzo); // recupera le variabili dal risultato ottenuto.


		while ($stmt->fetch()) {
		    echo "<tr class='empty-product-order'>";
		    echo "<td>$nome</td>";
		    echo "<td>$categoria</td>";
		    echo "<td>$quantita</td>";
		    echo "<td>$prezzo €</td>";
		    echo "</tr>";
		}
		$stmt->free_result();

		$stmt->close();
	}
} else {
   echo 'You are not authorized to access this page, please login. <br/>';
     header('Location: index.php');
}
?>