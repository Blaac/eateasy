<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
include 'secure.php';
include 'dbConnect.php';
sec_session_start();
if(mysqli_connect_errno()){
    die("conn failed: "
        . mysqli_connect_error()
        . " (" . mysqli_connect_errno()
        . ")");
}
if (isset($_POST['executeOrder'])){
  $cliente = $_SESSION['username'];
  $data = date("Y-m-d H:i:s");

  if (isset($_SESSION['address'])){
    $addr = $_SESSION['address'];
  } else {
    if ($stmt = $conn->prepare("SELECT indirizzo FROM Cliente WHERE Mail = ?")) {
      $stmt->bind_param('s', $cliente);
      $stmt->execute();
      $stmt->store_result();
      $stmt->bind_result($addr);
      $stmt->fetch();
    }
  }



  $ordine = $conn->prepare("INSERT INTO `ordine` (`Data`, `Cliente`,`Indirizzo`) VALUES (?, ?, ?)");

  $ordine->bind_param('sss', $data, $cliente, $addr);

  $ordine->execute();

  $orderid = $ordine->insert_id;


  if($dettaglio = $conn->prepare("INSERT INTO `dettaglioordine` (`Ordine`, `Prodotto`, `Quantita`, `Prezzo`, `Sconto`, `Tot`) VALUES (?, ?, ?, ?, ?, ?)")) { // assuming $mysqli is the connection
    if ($dettaglio->bind_param('iiiddd',$orderid,$itemid,$qtty,$price,$discount,$tot)){

      foreach ($_SESSION['cart_array'] as $each_item) {
        if ($each_item['item_id'] != null){
          $itemid = $each_item['item_id'];
          if ($stmt = $conn->prepare("SELECT Nome, Descrizione, Immagine, Prezzo, Categoria FROM Prodotto WHERE id = ?")) {
            $stmt->bind_param('i', $each_item['item_id']);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($name, $desc, $img, $price, $cat);
            $stmt->fetch();
          }
          $qtty = $each_item['quantity'];
          $discount = 0;
          $tot = $price * $qtty - $discount;
          $dettaglio->execute();
          // var_dump($dettaglio->error_list) ;
          // var_dump($dettaglio->error);
        }
      }
    }
  } else {
    $error = $conn->errno . ' ' . $conn->error;
    echo $error;
  }
  unset($_SESSION['cart_array']);
  unset($_POST['executeOrder']);
  $_SESSION['acquistato']= true;
} else {
  echo "Access denied";
}



 ?>
