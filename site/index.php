<?php

include 'dbConnect.php';
include 'secure.php';

sec_session_start();
if (login_check($conn)) {
	header('Location: indexLogged.php');
}
?>


<!DOCTYPE HTML>
<html lang="it-IT">

<head>
	<title>Eat Easy</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!-- CSS RESET-->
	<link rel="stylesheet" type="text/css" href="assets/css/Main/reset.css">
	 <!-- CSS IMPORT-->
	 <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
  	<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  	<!-- CSS SKELETON-->
  	<link rel="stylesheet" type="text/css" href="assets/css/basics/footer.css">
	<link rel="stylesheet" type="text/css" href="assets/css/Main/wrapper.css">
  	<link rel="stylesheet" href="assets/css/basics/main.css" >

</head>

<body class="landing">
	<!-- Page Wrapper -->
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header" class="alt">
			<h1><a href="index.html">EatEasy</a></h1>
			<nav id="nav">
				<ul>
					<li class="special">
						<a href="#menu" class="menuToggle"><span>Menu</span></a>
						<div id="menu">
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><a href="menu.php">Menù</a></li>
								<li><a href="formLogIn.php">Accedi</a></li>
								<li><a href="formSignUp.php">Registrati</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</nav>
		</header>

		<!-- Banner -->
		<section id="banner">
			<div class="inner">
				<h2>Eat Easy</h2>
				<p> Relax...<br /> take eateasy!<br /></p>
				<ul class="actions">
					<li><a href="formLogIn.php" class="button">Accedi</a></li>
					<li><a href="formSignUp.php" class="button special">Registrati</a></li>
				</ul>
			</div>
			<a href="#one" class="more scrolly">Il menù</a>
		</section>



		<!-- Three -->
		<section id="three" class="style3 wrapper special">
			<div class="inner">
				<header class="major">
					<h2>Il migliore street food romagnolo, a casa tua.</h2>
					<p>Perchè uscire di casa per mangiare una piadina?<br /> Ordina su EatEasy e goditela comodamente dal tuo divano!</p>
				</header>
			</div>
		</section>

		<!-- Two -->
		<div id="one" class="wrapper alt style2">
			<section class="spotlight">
				<div class="image"><img src="images/pic01.jpg" alt="" /></div>
				<div class="content">
					<h3>La vera piadina romagnola</h3>
					<p>La piadine sono preparate dalle sapienti mani delle nostre Azdore. L'impasto è soffice, leggero, digeribile e sopratutto romagnolo. Amiamo la semplicità e per questo utilizziamo solamente grani integrali e poco raffinati per garantirvi la massima qualità.</p>
				</div>
			</section>
			<section class="spotlight">
				<div class="image"><img src="images/pic02.jpg" alt="" /></div>
				<div class="content">
					<h3>Pasta</h3>
					 <p>In Romagna non abbiamo la pasta, abbiamo la Minestra. Le nostre minestre sono in linea con la tradizione romagnola; dalle lasagne ai passatelli, dai ravioli ai favolosi cappelletti della tradizione non manca nulla.</p>
				</div>
			</section>
			<section class="spotlight">
				<div class="image"><img src="images/pic03.jpg" alt="" /></div>
				<div class="content">
					<h3>Pizza</h3>
					 <p>La Pizza, ormai prodotto internazionale, icona delle spedizioni a domicilio, non poteva mancare nel nostro menu. Vieni a scoprire le nostre pizze speciali.</p>
				</div>
			</section>
		</div>

		<!-- Footer -->
		<footer id="footer">
		  <ul class="icons">
		    <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
		    <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
		    <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
		    <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
		    <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
		  </ul>
		  <ul class="copyright">
		    <li>&copy; Eat Easy</li>
		    <li>Design: Andrea Cardiota, Ruben Ceroni, Luca Giulianini</li>
		  </ul>
		</footer>
	</div>

	<!-- JQuery -->
	<script src="assets/js/Jquery/jquery.min.js"></script>
	<script src="assets/js/Jquery/jquery.scrollex.min.js"></script>
	<script src="assets/js/Jquery/jquery.scrolly.min.js"></script>


	<script src="assets/js/util.js"></script>
	<script src="assets/js/poll.js"></script>
</body>

</html>
