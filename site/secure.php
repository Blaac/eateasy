<?php
function sec_session_start() {
  session_save_path(realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/../session'));
        $session_name = 'sec_session_id'; // Imposta un nome di sessione
        $secure = false; // Imposta il parametro a true se vuoi usare il protocollo 'https'.
        $httponly = true; // Questo impedirà ad un javascript di essere in grado di accedere all'id di sessione.
        ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
        $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
        session_name($session_name); // Imposta il nome di sessione con quello prescelto all'inizio della funzione.
        session_start(); // Avvia la sessione php.
        session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
}


function login($mail, $password, $mysqli) {
   // Usando statement sql 'prepared' non sarà possibile attuare un attacco di tipo SQL injection.
   if ($stmt = $mysqli->prepare("SELECT Nome, Cognome, Indirizzo, Mail, Password FROM Cliente WHERE admin = 0 AND mail = ? LIMIT 1")) {
      $stmt->bind_param('s', $mail); // esegue il bind del parametro '$email'.
      $stmt->execute(); // esegue la query appena creata.
      $stmt->store_result();
      $stmt->bind_result($name, $surname, $address, $mail, $hashpass); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $stmt->fetch();

      if($stmt->num_rows == 1) { // se l'utente esiste

         // verifichiamo che non sia disabilitato in seguito all'esecuzione di troppi tentativi di accesso errati.
         if(checkbrute($mail, $mysqli) == true) {
            // Account disabilitato
            // Invia un e-mail all'utente avvisandolo che il suo account è stato disabilitato.
            return false;
         } else {
         if(password_verify($password, $hashpass)) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
            // Password corretta!
               $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente.

               // $user_id = preg_replace("/[^0-9]+/", "", $user_id); // ci proteggiamo da un attacco XSS
               // $_SESSION['user_id'] = $user_id;
               //$mail = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $mail); // ci proteggiamo da un attacco XSS
               $_SESSION['username'] = $mail;
               $_SESSION['login_string'] = $hashpass;//password_hash($password,
               $_SESSION['admin'] = 0;
               // Login eseguito con successo.
               return true;
         } else {
            // Password incorretta.
            // Registriamo il tentativo fallito nel database.
            $now = time();
            $mysqli->query("INSERT INTO login_attempts (mail, time) VALUES ('$mail', '$now')");
            return false;
         }
      }
      } else {
         // L'utente inserito non esiste.
         return false;
      }
   }
}

function loginAdmin($mail, $password, $mysqli) {
   // Usando statement sql 'prepared' non sarà possibile attuare un attacco di tipo SQL injection.
   if ($stmt = $mysqli->prepare("SELECT Nome, Cognome, Indirizzo, Mail, Password FROM Cliente WHERE admin = 1 AND mail = ? LIMIT 1")) {
      $stmt->bind_param('s', $mail); // esegue il bind del parametro '$email'.
      $stmt->execute(); // esegue la query appena creata.
      $stmt->store_result();
      $stmt->bind_result($name, $surname, $address, $mail, $hashpass); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $stmt->fetch();

      if($stmt->num_rows == 1) { // se l'utente esiste

          if(password_verify($password, $hashpass)) {
              // Password corretta!
              $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro
              $_SESSION['username'] = $mail;
              $_SESSION['login_string'] = $hashpass;//password_hash($password, PASSWORD_DEFAULT
              $_SESSION['admin'] = 1;
              // Login eseguito con successo.
              return true;
            } else {
              return false;
            }
        }
   }
}


function login_check($mysqli) {
   // Verifica che tutte le variabili di sessione siano impostate correttamente
   if(isset ($_SESSION['username'], $_SESSION['login_string'])) {
     $login_string = $_SESSION['login_string'];
     $username = $_SESSION['username'];
     $user_browser = $_SERVER['HTTP_USER_AGENT']; // reperisce la stringa 'user-agent' dell'utente.
     // echo $username;

     if ($stmt = $mysqli->prepare("SELECT password FROM cliente WHERE mail = ?")) {
        $stmt->bind_param('s', $username); // esegue il bind del parametro '$user_id'.
        $stmt->execute(); // Esegue la query creata.
        $stmt->store_result();
        if($stmt->num_rows == 1) { // se l'utente esiste
           $stmt->bind_result($password); // recupera le variabili dal risultato ottenuto.
           $stmt->fetch();
           // $login_check = hash('sha512', $password.$user_browser);
           if($password === $login_string) {
              // Login eseguito!!!!
              return true;
           } else {
             // echo 'pass errata';
              //  Login non eseguito
              return false;
           }
        } else {
          // echo 'non in db';
            // Login non eseguito
            return false;
        }
     } else {
       // echo 'query fallita';
        // Login non eseguito
        return false;
     }
   } else {
     // echo 'var non settate';
     // Login non eseguito
     return false;
   }
}

function checkAdmin() {
  if (isset($_SESSION['admin'])) {
    if ($_SESSION['admin']) {
      return true;
    } else {
      return false;
    }
  }
}

function checkbrute($mail, $mysqli) {
   // Recupero il timestamp
   $now = time();
   // Vengono analizzati tutti i tentativi di login a partire dalle ultime due ore.
   $valid_attempts = $now - (2 * 60 * 60);
   if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE mail = ? AND time > '$valid_attempts'")) {
      $stmt->bind_param('s', $mail);
      // Eseguo la query creata.
      $stmt->execute();
      $stmt->store_result();
      // Verifico l'esistenza di più di 5 tentativi di login falliti.
      if($stmt->num_rows > 5) {
         return true;
      } else {
         return false;
      }
   }
}
 ?>
