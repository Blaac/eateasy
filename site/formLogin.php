<?php
//Dichiarazione variabili per server
include 'dbConnect.php';
include 'secure.php';

//  && !empty($_POST['form-password'])) && isset($_POST['submit']
// isset($_POST['form-username'],$_POST['form-password'])
sec_session_start(); // usiamo la nostra funzione per avviare una sessione php sicura
if (login_check($conn)) {
  header('Location: ./indexLogged.php');
} else {

  if(!empty($_POST['form-username']) && !empty($_POST['form-password']) ) {
     $mail = $_POST['form-username'];
     $password = $_POST['form-password']; // Recupero la password criptata.
     if(login($mail, $password, $conn) == true) {
        $logged = 'logged';
        $_SESSION['logged'] = $logged;
        // Login eseguito
        header('Location: ./indexLogged.php');
     } else {
        if(loginAdmin($mail, $password, $conn) == true) {
            header('Location: ./indexLogged.php');
        } else {
            // echo 'Login failed';
            ?>
            <script type="text/javascript">
            alert('Login fallito')
            </script>
            <?php
          }
     }
  }
}
?>

<!DOCTYPE html>
<html lang="it-IT">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login - EatEasy</title>
  <!-- CSS IMPORT-->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <!-- CSS SKELETON-->
  <link rel="stylesheet" type="text/css" href="assets/css/Main/reset.css">
  <link rel="stylesheet" href="assets/css/basics/main.css" >
  <link rel="stylesheet" type="text/css" href="assets/css/basics/Footer.css">
  <!-- CSS FORM-->
  <link rel="stylesheet" href="assets/css/Form/form-elements.css">
  <link rel="stylesheet" href="assets/css/Form/form-transparency.css">
</head>

<body>

  <!-- Header -->
  <header id="header" class="alt">
    <h1><a href="index.html">EatEasy</a></h1>
    <nav id="nav">
      <ul>
        <li class="special">
          <a href="#menu" class="menuToggle"><span></span></a>
          <div id="menu">
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="menu.php">Menù</a></li>
              <li><a href="formLogIn.php">Accedi</a></li>
              <li><a href="formSignUp.php">Registrati</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </nav>
  </header>

  <!-- Top content -->
  <div class="top-content">
    <div class="inner-bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2 text">
            <h2><strong>Eat Easy</strong></h2>
            <div class="description">
              <p>Inserisci le tue credenziali ed effettua l'accesso per ordinare subito i tuoi piatti preferiti!</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
              <div class="form-top-left">
                <h3>Login</h3>
                <p>Inserisci email e password</p>
              </div>
              <div class="form-top-right">
                <em class="fa fa-lock"></em>
              </div>
            </div>
            <div class="form-bottom">
              <form  action="formLogIn.php" method="post" class="login-form">
                <div class="form-group">
                  <label class="sr-only" for="form-username">Email</label>
                  <input type="text" name="form-username" placeholder="Email" class="form-username form-control" id="form-username">
                </div>
                <div class="form-group">
                  <label class="sr-only" for="form-password">Password</label>
                  <input type="password" name="form-password" placeholder="Password" class="form-password form-control" id="form-password">
                </div>
                <button type="submit" class="btn" name="submit">Entra!</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer id="footer">
    <ul class="icons">
      <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
      <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
      <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
      <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
      <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
    </ul>
    <ul class="copyright">
      <li>&copy; Eat Easy</li>
      <li>Design: Andrea Cardiota, Ruben Ceroni, Luca Giulianini</li>
    </ul>
  </footer>

  <!-- Scripts -->
  <script src="assets/js/Jquery/jquery-1.11.1.min.js"></script>
  <script src="assets/js/Jquery/jquery.backstretch.min.js"></script>
  <script src="assets/js/Jquery/jquery.scrollex.min.js"></script>
  <script src="assets/js/Jquery/jquery.scrolly.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>

  <script src="assets/js/util.js"></script>
</body>

</html>
