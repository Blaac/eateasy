<?php
include 'dbConnectAdmin.php';
include 'secure.php';

sec_session_start();
if(login_check($conn) == true) {
    if (!checkAdmin()) {
        header('Location: ./indexLogged.php');
    }

    if (isset($_GET['username'])) {

         $sql = 'SELECT  O.Numero, C.Mail,  O.Data, O.Indirizzo, SUM(D.Quantita) as NumProdotti, SUM(D.Tot) as Tot, O.Spedito FROM cliente AS C, dettaglioordine AS D, ordine AS O WHERE C.Mail = O.Cliente AND O.Numero = D.Ordine AND C.mail = ? GROUP BY O.Numero ORDER BY O.Spedito AND O.Data';

        if(mysqli_connect_errno()){
            die("conn failed: "
                . mysqli_connect_error()
                . " (" . mysqli_connect_errno()
                . ")");
        }

        $stmt = $conn->prepare($sql);
        $stmt->bind_param('s',$_GET['username']);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($numero, $mail, $data, $indirizzo, $prodotti, $totale, $spedito); // risultato ottenuto.


    while ($stmt->fetch()) {
        echo "<tr class='empty-costumer-order'>";
        echo "<td class='numero-ordine'>$numero</td>";
        echo "<td>$mail</td>";
        echo "<td>$data</td>";
        echo "<td>$indirizzo</td>";
        echo "<td>$prodotti</td>";
        echo "<td>$totale €</td>";
        echo "<td>$spedito</td>";
        if ($spedito == 1) {
            echo "<td><button type='button' disabled class='send-button btn bottone-invio btn-xs'>inviato</td>";
        } else {
            echo "<td><button type='button' class='send-button btn bottone-invio btn-xs'>invia</td>";
        }
        echo "<td><button type='button' class='modal-button btn bottone-dettagli btn-xs' data-toggle='modal' data-target='#myModal'>dettagli</td>";
        echo "</tr>";
    }
    $stmt->free_result();

    $stmt->close();
    }

} else {
   echo 'You are not authorized to access this page, please login. <br/>';
     header('Location: index.php');
 }

?>
