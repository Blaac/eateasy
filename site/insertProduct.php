<?php

include 'dbConnect.php';
include 'secure.php';

sec_session_start();
if(login_check($conn) == true) {
	if (!checkAdmin()) {
		header('Location: ./indexLogged.php');
	}

	if (!empty($_POST["form-name"]) && !empty($_POST["form-desc"]) && !empty($_POST["form-prezzo"]) && !empty($_POST["form-categoria"])) {
			$name = $_POST["form-name"];
			$desc = $_POST["form-desc"];
			$prezzo = $_POST["form-prezzo"];
			$categoria = $_POST["form-categoria"];




			$target_dir = "images/cibo/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			// Check if image file is a actual image or fake image
		    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		    if($check !== false) {
		        //echo "File is an image - " . $check["mime"] . ".";
		        //echo "<br/>";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        echo "<br/>";
		        $uploadOk = 0;
		    }
			// Check if file already exists
			if (file_exists($target_file)) {
			    echo "Sorry, file already exists.";
			    echo "<br/>";
			    $uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 5000000000) {
			    echo "Sorry, your file is too large.";
			    echo "<br/>";
			    $uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" ) {
			    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
				echo "<br/>";
			    $uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
			    echo "Sorry, your file was not uploaded.";
			    echo "<br/>";
			// if everything is ok, try to upload file
			} else {
			    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			        //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";

			        $insert_stmt = $conn->prepare("INSERT INTO `prodotto`(`Nome`, `Descrizione`, `Immagine`, `Prezzo`, `Categoria`)  VALUES (?, ?, ?, ?, ?)");

				    $insert_stmt->bind_param('sssis', $name, $desc, $target_file, $prezzo, $categoria);

				    // Esegui la query ottenuta.
				    $insert_stmt->execute();
				    $insert_stmt->free_result();
				    $insert_stmt->close();
						$id = $insert_stmt->insert_id;

				    $conn = new mysqli(HOST, USER, PASSWORD, DATABASE);
				    $insert_stmt = $conn->prepare("INSERT INTO `prodottomenu`(`Id`, `Nome`, `Descrizione`, `Immagine`, `Prezzo`, `Categoria`)  VALUES (?, ?, ?, ?, ?, ?)");

				    $insert_stmt->bind_param('isssis',$id, $name, $desc, $target_file, $prezzo, $categoria);

				    // Esegui la query ottenuta.
				    $insert_stmt->execute();



					$insert_stmt->close();
			        header('Location: admin.php');





			    } else {
			        echo "Sorry, there was an error uploading your file.";
			        echo "<br/>";
			    }
			}

		}
	}
 ?>
