

$(document).ready(function(){

	$(document).on('click', '.button-acquista', function(event) {
		var id = $(event.target).parents(".product__info").children('.id-product').html();
		$.post("cartProcess.php", {addCart:id}, function(data, status){
			 if (status == 'success') {
				 console.log("added to cart: "+id);
			 }
		 });
	});

	$(document).on('click', '.button-rimuovi', function(event) {
		var id = $(event.target).parents(".product__info").children('.id-product').html();
		$.post("cartProcess.php", {removeAll:id}, function(data, status){
			 if (status == 'success') {
				 console.log("removed from cart: "+ id);
			 }
		 }).done();
	});

	$(document).on('click', '.button-login', function(event) {
		location.href = 'formLogin.php';
	});


	$(document).on('click', '.button-clear', function(event) {
		$.post("cartProcess.php", {emptycart:""}, function(data, status){
			 if (status == 'success') {
				 console.log("emptied cart");
			 }
		 }).done();
    });


	$(document).on('click', '.vai-al-carrello', function(event) {
		location.href = 'carrello.php';
	});


});
