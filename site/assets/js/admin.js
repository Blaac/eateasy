var input;

$(document).ready(function(){

	$(".costumer-order-button").on('click', function (event) {
		input = $('.costumerMail').val();
		if (input == '') {
			$.get("adminSearchAllOrders.php", {}, function(data, status){
					$( ".empty-costumer-order" ).empty();
		        	$( ".costumer-order-query" ).after(data);
		    });
		} else if (isNumeric(input)) {
			$.get("adminSearchOrderByNumber.php", {number:input}, function(data, status){
					$( ".empty-costumer-order" ).empty();
		        	$( ".costumer-order-query" ).after(data);
		    });
		} else {
			$.get("adminSearchOrder.php", {username:input}, function(data, status){
					$( ".empty-costumer-order" ).empty();
		        	$( ".costumer-order-query" ).after(data);
		    });
		}
	});



function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}



	$(document).on('click', '.modal-button', function() {
		var ordine = $(event.target).parents("tr").children('.numero-ordine').html();
		$.get("adminOrderDetail.php", {ordine:ordine}, function(data, status){
				$( ".empty-product-order" ).empty();
	        	$( ".dettaglio-ordine" ).after(data);
	        	$( ".numero-ordine-modal").html(ordine);
	    });
    });

    $(document).on('click', '.send-button', function() {
    	var ordine = $(event.target).parents("tr").children('.numero-ordine').html(); 
		$.get("adminSendOrder.php", {ordine:ordine}, function(data, status){
			if (status == 'success') {
				console.log(data);
				$('.bottone-invio').text('send');
			}
	    });
    });


    $(document).on('click', '.costumer-delete-button', function() {
    	var inputDelete = $(event.target).parents("tr").children('.nome-prodotto').html();
		$.get("deleteProduct.php", {productName:inputDelete}, function(data, status){
		});
		location.href = 'admin.php';
    });
});
