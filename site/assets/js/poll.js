Notification.requestPermission(function(status) {
});

$(document).ready(function() {
	setTimeout(doPoll, 4000);
	setTimeout(orderSentPoll, 2000);
});

function doPoll() {
	var noti = 1;
    $.post('sessionSetter.php', {notification:noti}, function(data) {
    	console.log(data);
		switch (data) {
				case "registrato":
					var notification = new Notification("Benvenuto su EatEasy!", {
						body: 'Comincia subito ad ordinare i tuoi piatti preferiti.',
						icon: 'images/notifiche/benvenuto.png'
					});
					break;
				case "loggato":
					var notification = new Notification("Bentornato!", {
						body: 'Ordina subito qualcosa.',
						icon: 'images/notifiche/benvenuto.png'
					});
					break;
				case "pagato":
					var notification = new Notification("Ordine confermato!", {
						body: 'Grazie! Un fattorino sarà a casa tua tra pochissimo.',
						icon: 'images/notifiche/grazie.png'
					});
					break;
				case "ordineInviato":
					var notification = new Notification("Order Sent", {
						body: 'L ordine è stato inviato.',
						icon: 'images/notifiche/ordineInviato.png'
					});
					break;
			}
    });
	setTimeout(doPoll, 2500);
}

function orderSentPoll() {
	 $.post('checkOrder.php', function(data) {
    	if (data != '') {
			var parsed = JSON.parse(data);
			if (parsed[0] == 'SENT') {
				var notification = new Notification("Send Order", {
					body: 'Sent order n' + parsed[1] + ' made by ' + parsed[2] + ' to ' + parsed[3],
					icon: 'images/notifiche/ordineConfermato.png'
				});
			}
		}
    });
	 setTimeout(orderSentPoll, 2000);
}