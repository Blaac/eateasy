var shippingRate = 0;
var fadeTime = 300;



$(document).ready(function() {

  var fadeTime = 300;
  /* Assign actions */
  $('.quantity input').change(function() {
    updateQuantity(this);
    updateCart(this);
  });

   $('select').on('change',function() {
    // updateQuantity(".quantity input");
    recalculateCart();
  });

  $('button.remove').click(function() {
    removeItem(this);
  });

  $('button.checkout-cta.btn').click(function () {
    modal.style.display = "block";
  })

  $(document).ready(function() {
    updateSumItems();
    // updateQuantity(this);
    recalculateCart();
  });


  /* Recalculate cart */
  function recalculateCart(onlyTotal) {
    var subtotal = 0;

    /* Sum up row totals */
    $('.basket-product').each(function() {
      subtotal += parseFloat($(this).children('.subtotal').text());
      // console.log("subt: "+subtotal);
    });

    /* Calculate totals */
    var total = subtotal;

    switch ($(".summary-delivery-selection").find(':selected').val()) {
      case "valPiedi":
        shippingRate = 0;
        break;
      case "valBici":
        shippingRate = 1;
        break;
      case "valMoto":
        shippingRate = 2;
        break;
      default:
        shippingRate = 0;
        break;
    }

    if (total == 0) {
      $('.checkout-cta').fadeOut(fadeTime);
    } else {
      $('.checkout-cta').fadeIn(fadeTime);
    }
    total = total + shippingRate;
    /*If switch for update only total, update only total display*/
      /* Update summary display. */
      $('.final-value').fadeOut(fadeTime, function() {
        $('#basket-subtotal').html(subtotal.toFixed(2) + "€");
        $('#basket-total').html(total.toFixed(2) + "€");

        $('.final-value').fadeIn(fadeTime);
      });
  }

  /* Update quantity */
  function updateQuantity(quantityInput) {
    /* Calculate line price */
    var productRow = $(quantityInput).parent().parent();
    var price = parseFloat(productRow.children('.price').text());
    // var id = productRow.children('.id-product').text();
    var quantity = $(quantityInput).val();
    var linePrice = price * quantity;
    /* Update line price display and recalc cart totals */
    productRow.children('.subtotal').each(function() {
      $(this).fadeOut(fadeTime, function() {
        $(this).text(linePrice.toFixed(2));
        recalculateCart();
        $(this).fadeIn(fadeTime);
      });
    });

    productRow.find('.item-quantity').text(quantity);
    updateSumItems();
  }



  function updateCart(quantityInput){
    var productRow = $(quantityInput).parent().parent();
    var id = productRow.children('.id-product').text();
    var quantity = $(quantityInput).val();
    //update qty in php
    $.post("cartProcess.php", {setNum:id,num:quantity}, function(data, status){
			 if (status == 'success') {
				 console.log("set to "+quantity+",id: "+id);
			 }
     });
  }

  function updateSumItems() {
    var sumItems = 0;
    $('.quantity input').each(function() {
      sumItems += parseInt($(this).val());
    });
    $('.total-items').text(sumItems);
  }

  /* Remove item from cart */
  function removeItem(removeButton) {
    /* Remove row from DOM and recalc cart total */
    var productRow = $(removeButton).parent().parent();
    var row = $(removeButton).parent();
    var id = row.children('.id-product').text();
    //update qty in php
    $.post("cartProcess.php", {removeAll:id}, function(data, status){
      if (status == 'success') {
        console.log("removed "+ id);
      }
     });
    productRow.slideUp(fadeTime, function() {
      productRow.remove();
      recalculateCart();
      updateSumItems();
    });
  }

  var modal = document.getElementById('myModal');



  $('button.cancel.btn').click( function() {
      modal.style.display = "none";
  })

  $('button.confirm.btn').click( function() {
    checkout();

  })


  $('span.close').click( function() {
      modal.style.display = "none";
  })

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal) {
          modal.style.display = "none";
      }
  }

  function checkout() {
    $.post("orderConfirm.php", {executeOrder:""}, function(data, status){
      if (status == 'success') {
        location.href = 'index.php';
      }
     });

  }
});
