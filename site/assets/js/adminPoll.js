Notification.requestPermission(function(status) {
});

$(document).ready(function() {
	doPoll();
});

function doPoll() {
	var noti = 1;
    $.post('adminNotifyOrder.php', {notification:noti}, function(data) {
    	if (data != '') {
    		var parsed = JSON.parse(data);
    		if (parsed[0] == 'TOSEND') {
			var notification = new Notification("Send Order", {
				body: 'Send order n' + parsed[2] + ' made by ' + parsed[1],
				icon: 'images/notifiche/ordineArrivato.png'
			});
    			
    		}
			setTimeout(doPoll, 60000);
		}
    });
}