

      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var  autocomplete;
      var addr;
      var update = false;

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode'],strictBounds:true,});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', getInAddress);

        $('button#buttondef').click(function () {
          location.href = 'menu.php';
        })

        $('button#buttoncurr').click(function () {
          if (addr != undefined){
            if($('input#updatedef').is(':checked')){
              update = true;
            } else {
              var update = false;
            }
            $.post("cartProcess.php", {address:addr,update:update}, function(data, status){
              if (status == 'success') {
                location.href = 'menu.php';
              }
            });
          }
        })
      }

      function getInAddress() {

        //  var address_components = results[0].address_components;
         var place = autocomplete.getPlace();
         var address_components = place.address_components;
         var components={};
         jQuery.each(address_components, function(k,v1) {
           jQuery.each(v1.types, function(k2, v2){
             components[v2]=v1.long_name;
           });
         });

         if(components.street_number === undefined){
           fail("Civico mancante");
         } else {
           if (components.locality !== 'Cesena'){
             fail("Città errata");
           } else {
             addr = place.formatted_address;
             console.log(addr);
             // var num = components.street_number
             // var via = components.route
             // var provincia =components.administrative_area_level_2;
             // var citta = components.administrative_area_level_3;
             // var cap = components.postal_code
             // console.log(num);
             // console.log(via);
             // console.log(provincia);
             // console.log(citta);
             // console.log(cap);

           }
         }

      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        // if (navigator.geolocation) {
          // navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat:44.14918, //position.coords.latitude,
              lng: 12.1924236//position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: 5000
            });
            autocomplete.setBounds(circle.getBounds());
          // });
        // }
      }

      function fail(err) {
        document.getElementById('autocomplete').value = "Indirizzo non valido, " + err
      }
