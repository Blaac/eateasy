<?php 
include 'dbConnectAdmin.php';

function allProduct() {
	global $conn;
	sec_session_start();
	if(login_check($conn) == true) {
	    if (!checkAdmin()) {
	        header('Location: ./indexLogged.php');
	    }

	    $sql = 'SELECT P.Nome FROM prodottomenu AS P';

	    if(mysqli_connect_errno()){
	        die("conn failed: "
	            . mysqli_connect_error()
	            . " (" . mysqli_connect_errno()
	            . ")");
	    }

	    $stmt = $conn->prepare($sql);
	    $stmt->execute();
	    $stmt->store_result();
	    $stmt->bind_result($nome); // risultato ottenuto.


	    while ($stmt->fetch()) {
	        echo "<tr class='empty-product'>";
	        echo "<td class='nome-prodotto'>$nome</td>";
	        echo "<td class='delete-button-td'><button type='button' class='btn costumer-delete-button btn-xs'>elimina</td>";
	        echo "</tr>";
	    }
	    $stmt->free_result();

	    $stmt->close();
	} else {
	   echo 'You are not authorized to access this page, please login. <br/>';
	     header('Location: index.php');
	 }

}



 ?>