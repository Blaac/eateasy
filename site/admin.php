<?php

include 'dbConnectAdmin.php';
include 'secure.php';
include 'productMenu.php';

$username;

$nome;
$cognome;
$indirizzo;
$mail;


sec_session_start();
if(login_check($conn) == true) {
	if (!checkAdmin()) {
		header('Location: ./indexLogged.php');
	}
	$username = $_SESSION['username'];

if ($stmt = $conn->prepare("SELECT cliente.nome, cliente.cognome, cliente.indirizzo, cliente.mail FROM cliente WHERE mail = ?")) {
		$stmt->bind_param('s', $username); // esegue il bind del parametro '$user_id'.
		$stmt->execute(); // Esegue la query creata.
		$stmt->store_result();
		$stmt->bind_result($nome, $cognome, $indirizzo, $mail); // recupera le variabili dal risultato ottenuto.
		while ($stmt->fetch()) {
		}
		$stmt->free_result();
    $stmt->close();
}
?>

<!DOCTYPE html>
<html lang="it-IT">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo "$nome 's area" ?></title>

  <!-- CSS RESET-->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
  <link rel="stylesheet" type="text/css" href="assets/css/Main/reset.css">
  <!-- CSS IMOPORT-->
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <!-- CSS-->
  <link rel="stylesheet" type="text/css" href="assets/css/Admin/admin.css">
  <link rel="stylesheet" type="text/css" href="assets/css/Carrello/footer-bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/Basics/lateralBar.css">


</head>

<body>
    <header id="header" class="my-header">
      <h1 class="titolo panel-heading"><span class="icon cart-moving fa fa-user"></span> Ciao! <?php echo $nome ?> </h1>
       <nav id="nav">
       <ul>
         <li class="special">
           <a href="#menu" class="menuToggle"><span></span></a>
           <div id="menu">
             <ul>
               <li><a href="index.php">Home</a></li>
               <li><a href="personal.php">Area Personale</a></li>
               <li><a href="menu.php">Menù</a></li>
               <li><a href="carrello.php">Carrello</a></li>
               <li><a href='admin.php'>Admin</a></li>
               <li><a href="logout.php">Logout</a></li>
             </ul>
           </div>
         </li>
       </ul>
     </nav>
    </header>

<main>

<div class="col-sm-5 admin-panels">

    <section class="container-fluid">
      <div class="pannello-user">
          <h2>Inserisci Prodotto <span class="fa fa-pencil icon"></span></h2>
            <div class="form-bottom">
              <form  action="insertProduct.php" method="post" class="login-form" enctype="multipart/form-data">
                  <div class="form-group">
                    <label class="sr-only" for="form-name">Nome</label>
                    <input type="text" name="form-name" placeholder="Nome" class="form-name form-control" id="form-name">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="form-desc">Descrizione</label>
                    <input type="text" name="form-desc" placeholder="Descrizione" class="form-desc form-control" id="form-desc">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="form-prezzo">Prezzo</label>
                    <input type="text" name="form-prezzo" placeholder="Prezzo" class="form-prezzo form-control" id="form-prezzo">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="form-immagine">Immagine</label>
                    <input type="file" name="fileToUpload" id="form-immagine">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="form-categoria">Categoria</label>
                    <select name="form-categoria" id="form-categoria">
                      <?php

                      $sql = 'SELECT C.Nome  FROM categoria AS C';
                      $stmt = $conn->prepare($sql);
                      $stmt->execute();
                      $stmt->store_result();
                      if ($stmt->num_rows <= 0) {
                          echo "nessuna categoria trovata";
                      } else {
                        $stmt->bind_result($nome);
                        while ($stmt->fetch()) {
                            echo "<option value='{$nome}'>$nome</option>";
                        }
                        $stmt->free_result();
                        $stmt->close();
                      }
                      ?>
                    </select>
                  </div>
                <button type="submit" class="btn">Inserisci!</button>
            </form>
          </div>
      </div>
    </section>

</div>


<div class="col-sm-7 admin-panels">
    <section class="container-fluid">
    	<div class="pannello-user">
	        <h2>Cerca Ordine <span class="fa fa-table icon"></span></h2>
		        <div class="container-fluid admin-input">
		        	<label class="sr-only" for="costumerOrder">Cliente:</label>
		        	<input class="costumerMail" type="email" placeholder="Mail or Order Num" name="costumerOrder" id="costumerOrder"/>
		        	<button class="btn costumer-order-button" name="submit">Cerca!</button>
		        </div>
            <div class="table-responsive">
              <table class="table">
                <tr class="costumer-order-query">
                  <th>#</th>
                  <th>Mail</th>
                  <th>Data</th>
                  <th>Indirizzo</th>
                  <th>Prodotti</th>
                  <th>Prezzo</th>
                  <th>Spedito</th>
                </tr>
              </table>
            </div>
    	</div>
    </section>


    <section class="container-fluid">
      <div class="pannello-user">
          <h2>Rimuovi Prodotto Menu <span class="fa fa-table icon"></span></h2>
            <div class="table-responsive">
              <table class="table">
                <tr class="delete-product-menu">
                  <th>Prodotto</th>
                </tr>
                <?php  allProduct(); ?>
              </table>
            </div>
      </div>
    </section>
</div>

   <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content pannello-user pannello-modal">
          <h3 class="modal-title">Ordine <span class="numero-ordine-modal"></span> <span class="fa fa-table icon"></span></h3>
          <div class="table-responsive">
            <table class="table">
              <tr class="dettaglio-ordine">
                <th>Nome</th>
                <th>Categoria</th>
                <th>Quantità</th>
                <th>Prezzo</th>
              </tr>
            </table>
          </div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
      </div>

    </div>
  </div>
</main>

  <!-- Footer -->
    <footer class="col-sm-12 footer">
      <ul class="icons">
        <li><a href="#" class="icon fa fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa fa-instagram"><span class="label">Instagram</span></a></li>
        <li><a href="#" class="icon fa fa-dribbble"><span class="label">Dribbble</span></a></li>
        <li><a href="#" class="icon fa fa-envelope-o"><span class="label">Email</span></a></li>
      </ul>
      <ul class="copyright">
        <li>&copy; Eat Easy</li>
        <li>Design: Andrea Cardiota, Ruben Ceroni, Luca Giulianini</li>
      </ul>
    </footer>

   <!-- Scripts -->
  <script src="assets/js/Jquery/jquery-1.11.1.min.js"></script>
  <script src="assets/js/Jquery/jquery.backstretch.min.js"></script>
  <script src="assets/js/Jquery/jquery.scrollex.min.js"></script>
  <script src="assets/js/Jquery/jquery.scrolly.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>

  <script src="assets/js/admin.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/adminPoll.js"></script>

</body>
</html>

<?php
} else {
   echo 'Non hai le autorizzazioni necessarie per accedere a questo contenuto. <br/>';
	 header('Location: index.php');
 }
 ?>
