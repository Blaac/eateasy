<?php

//Dichiarazione variabili per server
include 'dbConnect.php';
include 'secure.php';
// Recupero la password criptata dal form di inserimento.

sec_session_start();
if (login_check($conn)) {
  header('Location: ./indexLogged.php');
} else {

  if(!empty($_POST["form-name"]) && !empty($_POST["form-surname"]) && !empty($_POST["form-indirizzo"])
                                 && !empty($_POST["form-email"]) && !empty($_POST["form-password"])) {
    //preparazione query
    $password = $_POST['form-password'];
    $nome = $_POST["form-name"];
    $cognome = $_POST["form-surname"];
    $indirizzo = $_POST["form-indirizzo"];
    $email = $_POST['form-email'];


    $stmt = $conn->prepare("SELECT * FROM cliente WHERE Mail = ?");
    $stmt->bind_param('s', $email); // esegue il bind del parametro '$user_id'.
    $stmt->execute(); // Esegue la query creata.
    $stmt->store_result();
    if($stmt->num_rows > 0) {
      ?>
      <script type="text/javascript">
      alert('Utente già registrato')
      </script>

      <?php
    } else {
      $conn = new mysqli(HOST, USER, PASSWORD, DATABASE);
      $pass = $password;
      $password = password_hash($password, PASSWORD_DEFAULT, ['cost' => 10]);
      $insert_stmt = $conn->prepare("INSERT INTO Cliente (Nome, Cognome, Indirizzo, Mail, Password) VALUES (?, ?, ?, ?, ?)");
      $insert_stmt->bind_param('sssss', $nome, $cognome, $indirizzo, $email, $password);
      // Esegui la query ottenuta.
      $insert_stmt->execute();
      // header('Location: ./formLogIn.php');
      $signed = 'signed';
      $_SESSION['signed'] = $signed;
      if(login($email, $pass, $conn) == true) {
         $logged = 'logged';
         $_SESSION['logged'] = $logged;
         // Login eseguito
         header('Location: ./indexLogged.php');
      } else {
         if(loginAdmin($mail, $password, $conn) == true) {
             header('Location: ./indexLogged.php');
         } else {
             // echo 'Login failed';
             ?>
             <script type="text/javascript">
             alert('Utente già registrato')
             </script>

             <?php
           }
      }
    }
  }
}
?>

<!DOCTYPE html>
<html lang="it-IT">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Registrati - EatEasy</title>
  <!-- CSS IMPORT-->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <!-- CSS SKELETON-->
  <link rel="stylesheet" href="assets/css/basics/main.css" />
  <link rel="stylesheet" type="text/css" href="assets/css/basics/Footer.css">
  <!-- CSS FORM-->
  <link rel="stylesheet" href="assets/css/Form/form-elements.css">
  <link rel="stylesheet" href="assets/css/Form/form-transparency.css">
  <link rel="stylesheet" href="assets/css/Basics/modal.css">
</head>

<body>

  <!-- Header -->
  <header id="header" class="alt">
    <h1><a href="index.html">EatEasy</a></h1>
    <nav id="nav">
      <ul>
        <li class="special">
          <a href="#menu" class="menuToggle"><span></span></a>
          <div id="menu">
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="menu.php">Menù</a></li>
              <li><a href="formLogIn.php">Accedi</a></li>
              <li><a href="formSignUp.php">Registrati</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </nav>
  </header>

  <div id="myModal" class="modal">
  <div class="modal-content">
  <span class="close">&times;</span>
  <p>Sei sicuro?</p>
  <button class="cancel btn">Annulla</button>
  <button class="confirm btn">Conferma</button>
  </div>
  </div>

  <!-- Top content -->
  <div class="top-content">
    <div class="inner-bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2 text">
            <h2><strong>Eat Easy</strong></h2>
            <div class="description">
              <p>Registrati gratuitamente ed inizia subito ad ordinare i tuoi piatti preferiti!</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
              <div class="form-top-left">
                <h3>Registrati</h3>
                <p>Compila i seguenti campi:</p>
              </div>
              <div class="form-top-right">
                <em class="fa fa-lock"></em>
              </div>
            </div>
            <div class="form-bottom">
              <form  action="formSignUp.php" method="post" class="login-form">
                  <div class="form-group">
                    <label class="sr-only" for="form-name">Nome</label>
                    <input type="text" name="form-name" placeholder="Nome" class="form-name form-control" id="form-name">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="form-surname">Cognome</label>
                    <input type="text" name="form-surname" placeholder="Cognome" class="form-surname form-control" id="form-surname">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="form-indirizzo">Indirizzo</label>
                    <input type="text" name="form-indirizzo" placeholder="Indirizzo" class="form-indirizzo form-control" id="form-indirizzo">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="form-email">Email</label>
                    <input type="email" name="form-email" placeholder="Email" class="form-email form-control" id="form-email">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="form-password">Password</label>
                    <input type="password" name="form-password" placeholder="Password" class="form-password form-control" id="form-password">
                  </div>
                <button type="submit" class="btn">Entra!</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer id="footer">
    <ul class="icons">
      <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
      <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
      <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
      <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
      <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
    </ul>
    <ul class="copyright">
      <li>&copy; Eat Easy</li>
      <li>Design: Andrea Cardiota, Ruben Ceroni, Luca Giulianini</li>
    </ul>
  </footer>

  <!-- Scripts -->
  <script src="assets/js/Jquery/jquery-1.11.1.min.js"></script>
  <script src="assets/js/Jquery/jquery.backstretch.min.js"></script>
  <script src="assets/js/Jquery/jquery.scrollex.min.js"></script>
  <script src="assets/js/Jquery/jquery.scrolly.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>

	<script src="assets/js/util.js"></script>
</body>

</html>
