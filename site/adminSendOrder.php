<?php

include 'dbConnectAdmin.php';
include 'secure.php';

sec_session_start();
if(login_check($conn) == true) {
    if (!checkAdmin()) {
        header('Location: ./indexLogged.php');
    }

    if (isset($_GET['ordine'])) {

        $sql = 'SELECT  O.Numero, C.Mail,  O.Data, O.Indirizzo, SUM(D.Quantita) as NumProdotti, SUM(D.Tot) as Tot, O.Spedito FROM cliente AS C, dettaglioordine AS D, ordine AS O WHERE C.Mail = O.Cliente AND O.Numero = D.Ordine AND O.Numero = ?  GROUP BY O.Numero ORDER BY O.Spedito AND O.Data';

        if(mysqli_connect_errno()){
            die("conn failed: "
                . mysqli_connect_error()
                . " (" . mysqli_connect_errno()
                . ")");
        }

        $stmt = $conn->prepare($sql);
        $stmt->bind_param('i', $_GET['ordine']);
        $stmt->execute();
        $stmt->store_result();
         $stmt->bind_result($numero, $mail, $data, $indirizzo, $prodotti, $totale, $spedito); // risultato ottenuto.


        while ($stmt->fetch()) {
        }
        $stmt->free_result();

        $stmt->close();

        if ($spedito == 0) {
            $conn = new mysqli(HOST, USER, PASSWORD, DATABASE);

            $sql = "UPDATE ordine SET Spedito = b'1' WHERE ordine.Numero = ?";

            if(mysqli_connect_errno()){
                die("conn failed: "
                    . mysqli_connect_error()
                    . " (" . mysqli_connect_errno()
                    . ")");
            }

            $stmt = $conn->prepare($sql);
            $stmt->bind_param('i', $_GET['ordine']);
            $stmt->execute();
            $stmt->free_result();
            $stmt->close();
            
        }
    }
} else {
   echo 'You are not authorized to access this page, please login. <br/>';
     header('Location: index.php');
 }

?>