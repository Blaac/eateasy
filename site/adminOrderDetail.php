<?php
include 'secure.php';
include 'dbConnectAdmin.php';

sec_session_start();
if(login_check($conn) == true) {
    if (!checkAdmin()) {
        header('Location: ./indexLogged.php');
    }
    if (isset($_GET['ordine'])) {

        $sql = 'SELECT P.Nome, P.categoria, D.quantita, P.prezzo FROM prodotto as P, dettaglioordine AS D, ordine AS O WHERE  O.Numero = D.Ordine AND P.Id = D.Prodotto AND O.Numero = ?';


        if(mysqli_connect_errno()){
            die("conn failed: "
                . mysqli_connect_error()
                . " (" . mysqli_connect_errno()
                . ")");
        }



        $stmt = $conn->prepare($sql);
        $stmt->bind_param('i',$_GET['ordine']);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($nome, $categoria, $quantita, $prezzo); // recupera le variabili dal risultato ottenuto.


        while ($stmt->fetch()) {
            echo "<tr class='empty-product-order'>";
            echo "<td>$nome</td>";
            echo "<td>$categoria</td>";
            echo "<td>$quantita</td>";
            echo "<td>$prezzo €</td>";
            echo "</tr>";
        }
        $stmt->free_result();

        $stmt->close();
    }
} else {
   echo 'You are not authorized to access this page, please login. <br/>';
     header('Location: index.php');
 }
?>