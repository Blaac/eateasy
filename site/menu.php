
<?php

include 'dbConnect.php';
// $conn = new mysqli($servername, $username, $password, $database);
include 'secure.php';
sec_session_start();
$logged = false;
if(login_check($conn) == true) {
	$logged = true;
}

if($conn->connect_error) {
	die("Connection failed" . $conn->connect_error);
}

$sql = "SELECT * FROM prodottomenu ORDER BY Categoria";
$result = $conn->query($sql);
// $_SESSION['prodotti'] = null;
//var_dump(($_SESSION['cart_array']));

?>

<!DOCTYPE html>
<html lang="it-IT" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Menù - Eat Easy</title>
		<!-- CSS -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">

		<link rel="stylesheet" type="text/css" href="assets/css/Menu/elem-menu.css" />
		<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/Menu/menu.css" />
	</head>
	<body>
		<!-- Compare basket -->
		<div class="compare-basket">
			<button class="action action--button action--compare"><emclass="fa fa-check"></em><span class="action__text">Compare</span></button>
		</div>
		<!-- Main view -->
		<div class="view">
			<!-- Blueprint header -->
			<header class="bp-header cf">
				<span id="titolo">Eat Easy <span class="bp-icon bp-icon-about" data-content="In questa sezione puoi visualizzare l'intero menù attualmente disponibile su Eat Easy ed inserire prodotti nel carrello, pronti per essere ordinati."></span></span>
				<h1 id="menu">Menù</h1>
				<nav>
					<a id="prev" href="index.php" class="bp-icon fa fa-arrow-left" data-info="Home"><span>Home</span></a>
					<!--a href="" class="bp-icon bp-icon-next" data-info="next Blueprint"><span>Next Blueprint</span></a-->
					<?php
						if($logged) { ?>

					<a id="casa" class="bp-icon fa fa-cart-plus vai-al-carrello" data-info="Carrello"><span>Vai al carrello</span></a>

				</nav>
					<button class="action action--button action--buy button-clear"><emclass="fa fa-trash"></em><span class="action__text">Svuota carrello</span></button>
					<?php } ?>
			</header>
			<!-- Product grid -->
			<section class="grid">
				<!-- Products -->
				<?php
				if ($result->num_rows > 0) {
					// output data of each row
					while($row = $result->fetch_assoc()) {
						$src = $row['Immagine'];
						$alt = $row['Id'];
						?>
						<div class="product">
							<div class="product__info">
								<img class="product__image" src="<?php echo $src; ?>" alt="<?php echo $alt?>" />
								<h2 class="product__title"><?php echo $row['Nome'];?></h2>
								<span class="product__year extra highlight"></span>
								<span class="product__region extra highlight"></span>
								<span class="product__varietal extra highlight"><?php echo $row['Categoria']; ?></span>
								<span class="product__alcohol extra highlight"></span>
								<span class="product__price highlight">€ <?php echo $row['Prezzo']; ?></span>
								<span class="id-product" style="display:none"><?php echo $alt; ?></span>
								<?php  if($logged) { ?>
								<button class="action action--button action--buy button-acquista"><em class="fa fa-shopping-cart"></em> Aggiungi al carrello</button>
								<button class="action action--button action--buy button-rimuovi"><em class="fa fa-shopping-cart"></em> Rimuovi dal carrello</button>

								<?php } else { ?>
									<button class="action action--button action--buy button-login"><em class="fa fa-shopping-cart"></em>Effettua il login per ordinare</button>
								<?php }	?>
							</div>
							<label style="" class="action action--compare-add fa fa-plus"><input class="check-hidden" type="checkbox" /><emclass="fa fa-plus"></em><emclass="fa fa-check"></em><span class="action__text action__text--invisible">Add to compare</span></label>
						</div>

					<?php
					}
				}
				 ?>
			</section>
		</div><!-- /view -->
		<!-- product compare wrapper -->
		<section class="compare">
			<h2 style="display: none">Compare</h2>
			<button class="action action--close"><em class="fa fa-remove"></em><span class="action__text action__text--invisible">Close comparison overlay</span></button>
		</section>

		 <!-- Scripts -->
		<script src="assets/js/Jquery/jquery-1.11.1.min.js"></script>
		<script src="assets/js/Jquery/jquery.backstretch.min.js"></script>
		<script src="assets/js/Jquery/jquery.scrollex.min.js"></script>
		<script src="assets/js/Jquery/jquery.scrolly.min.js"></script>
		<script src="assets/bootstrap/js/bootstrap.min.js"></script>

		<script src="assets/js/modernizr.custom.js"></script>
		<script src="assets/js/classie.js"></script>
		<script src="assets/js/mainCart.js"></script>
		<script src="assets/js/menu.js"></script>
	</body>
</html>
