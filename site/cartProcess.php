<?php
include 'dbConnect.php';
include 'secure.php';

sec_session_start();
if(login_check($conn) == true) {
    //add one item
    if (isset($_POST['addCart'])) {

      $pid = $_POST['addCart'];
      $wasFound = false;

      if (!isset($_SESSION["cart_array"]) || count($_SESSION["cart_array"]) < 1) {
        $_SESSION["cart_array"] = array(array("item_id" => $pid, "quantity" => 1));
      } else {
        foreach ($_SESSION["cart_array"] as &$each_item) {
          if ($each_item['item_id'] == $pid) {
            $each_item['quantity'] += 1;
            $wasFound = true;
            break;
          }
        }
        if ($wasFound == false) {
          array_push($_SESSION["cart_array"], array("item_id" => $pid, "quantity" => 1));
        }
      }

    }
    //set item number to value
    elseif (isset($_POST['setNum'])) {

      $pid = $_POST['setNum'];
      $num = $_POST['num'];
      $wasFound = false;

      if (!isset($_SESSION["cart_array"]) || count($_SESSION["cart_array"]) < 1) {
        $_SESSION["cart_array"] = array(array("item_id" => $pid, "quantity" => $num));
      } else {
        foreach ($_SESSION["cart_array"] as &$each_item) {
          if ($each_item['item_id'] == $pid) {
            $each_item['quantity'] = $num;
            $wasFound = true;
            break;
          }
        }
        if ($wasFound == false) {
          array_push($_SESSION["cart_array"], array("item_id" => $pid, "quantity" => $num));
        }
      }
    }
    //remove one item
    elseif (isset($_POST['removeCart'])) {
				$pid = $_POST['removeCart'];
				if (isset($_SESSION["cart_array"]) || count($_SESSION["cart_array"]) < 1) {
					foreach ($_SESSION["cart_array"] as &$each_item) {
						if ($each_item['item_id'] == $pid) {
							$each_item['quantity'] -= 1;
							if ($each_item['quantity'] == 0){
								$each_item = null; //lascia puntatori nulli, da risolvere
							}
						}
					}
        }
      }
      //remove all items of one kind
     elseif (isset($_POST['removeAll'])) {
      $pid = $_POST['removeAll'];
      if (isset($_SESSION["cart_array"]) || count($_SESSION["cart_array"]) < 1) {
        foreach ($_SESSION["cart_array"] as &$each_item) {
          if ($each_item['item_id'] == $pid) {
              $each_item = null; //lascia puntatori nulli, da risolvere
          }
        }
      }
    }
    //empty cart
    elseif (isset($_POST['emptycart'])) {
      unset($_SESSION['cart_array']);
    }
    elseif (isset($_POST['address'])) {
      if ($_POST['update']){
        $ordine = $conn->prepare("UPDATE cliente SET Indirizzo = ?  WHERE cliente.Mail = ?");

        $ordine->bind_param('ss', $_POST['address'], $_SESSION['username']);

        $ordine->execute();
      }
      $_SESSION['address'] = $_POST['address'];
    }

}

?>
