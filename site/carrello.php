
<?php
include 'dbConnect.php';
include 'secure.php';

sec_session_start();
if(login_check($conn) == true) {

    }

    if($conn->connect_error) {
    	die("Connection failed" . $conn->connect_error);
    }


?>


<!DOCTYPE html>
<html lang="it-IT">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Carrello</title>

  <!-- CSS RESET-->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
  <link rel="stylesheet" type="text/css" href="assets/css/Main/reset.css">
  <!-- CSS IMOPORT-->
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/Basics/modal.css">
  <!-- CSS-->
  <link rel="stylesheet" type="text/css" href="assets/css/Carrello/carrello.css">
  <link rel="stylesheet" type="text/css" href="assets/css/Carrello/footer-bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/Basics/lateralBar.css">


</head>

<body>
    <header id="header" class="my-header">
      <h1 class="titolo panel-heading">Carrello <span class="icon cart-moving fa fa-cart-plus"></span></h1>
      <nav id="nav">
       <ul>
         <li class="special">
           <a href="#menu" class="menuToggle"><span></span></a>
           <div id="menu">
             <ul>
               <li><a href="index.php">Home</a></li>
               <li><a href="personal.php">Area Personale</a></li>
               <li><a href="menu.php">Menù</a></li>
               <li><a href="carrello.php">Carrello</a></li>
               <?php if(checkAdmin()) {
               echo "<li><a href='admin.php'>Admin</a></li>";
               } ?>
               <li><a href="logout.php">Logout</a></li>
             </ul>
           </div>
         </li>
       </ul>
     </nav>
    </header>


  <main>


    <div id="myModal" class="modal">
    <!-- Modal content -->
      <div class="modal-content">
        <span class="close">&times;</span>

        <label>
          <input type="radio" name="paypal" value="small" />
          <img src="images/payment/paypal.png">
        </label>
        <label>
          <input type="radio" name="paypal" value="small" />
          <img src="images/payment/mastercard.png">
        </label>
        <label>
          <input type="radio" name="paypal" value="small" />
          <img src="images/payment/visa.png">
        </label>

        <!-- <input type="image" src="images/payment/paypal.png" /> -->
        <!-- <input type="image" src="images/payment/maestro.png" /> -->
        <!-- <input type="image" src="images/payment/mastercard.png" />
        <input type="image" src="images/payment/visa.png" /> -->
        <p>Seleziona modalità di pagamento</p>
        <button class="cancel btn">Annulla</button>
        <button class="confirm btn">Conferma</button>
      </div>
    </div>



    <aside class="col-sm-5 col-sm-push-7 pannello-carrello">
      <div class="carrello">
        <h2>Checkout <span class="fa fa-credit-card fa-fw icon"></span></h2>
        <div class="summary-total-items"><span class="total-items"></span> Articoli nel carrello</div>
        <div class="summary-subtotal">
          <div class="subtotal-title">Subtotale</div>
          <div class="subtotal-value final-value" id="basket-subtotal">0.00</div>
        </div>
        <div class="summary-delivery">
          <label for="modalità" class="visuallyhidden">Modalità
          </label>
            <select name="delivery-collection" class="summary-delivery-selection" id="modalità">
              <option value="0" selected="selected">Seleziona modalità spedizione</option>
              <option value="valPiedi">Fattorino a piedi (gratis)</option>
              <option value="valBici">Fattorino in bici (+1€)</option>
              <option value="valMoto">Fattorino in moto (+2€)</option>
            </select>
        </div>
        <div class="summary-total">
          <div class="total-title">Totale</div>
          <div class="total-value final-value" id="basket-total">0.00</div>
        </div>
        <div class="summary-checkout">
          <button class="checkout-cta btn">Conferma ordine</button>
        </div>
      </div>
    </aside>

        <div class=" col-sm-7 col-sm-pull-5 design-pannello-prodotti">
          <section class="col-sm-10 container-fluid pannello-prodotti">
            <h2 style="display: none;">prodotti</h2>
<?php
foreach ($_SESSION['cart_array'] as $each_item) {
  if ($each_item['item_id'] != null){
    if ($stmt = $conn->prepare("SELECT Nome, Descrizione, Immagine, Prezzo, Categoria FROM Prodotto WHERE id = ?")) {
      $stmt->bind_param('i', $each_item['item_id']);
      $stmt->execute();
      $stmt->store_result();
      $stmt->bind_result($name, $desc, $img, $price, $cat);
      $stmt->fetch();
    }

        ?>

        <article class="prodotto container-fluid col-sm-12">
          <div class="col-sm-3 pannello-photo">
            <img src="<?php echo $img ?>" alt="<?php echo $desc ?>" class="photo">
          </div>
          <div class="basket-product descrizione col-sm-9">
            <h3><span class="item-quantity"><?php echo $each_item['quantity'] ?></span> x <?php echo $name ?></h3>
            <span class="id-product" style="display:none;"><?php echo $each_item['item_id']; ?></span>
            <span class="price"><?php echo $price ?></span>€ <br>
            <div class="quantity">
              <input type="number" value="<?php echo $each_item['quantity'] ?>" min="1" class="quantity-field">
            </div>
            <span class="subtotal"><?php echo $price * $each_item['quantity']?></span>€ <br>
            <button class="btn remove">Remove</button>
          </div>
        </article>


        <?php
    }
  }
?>
          </section>
        </div>


  </main>


  <!-- Footer -->
    <footer class="col-sm-12 footer">
      <ul class="icons">
        <li><a href="#" class="icon fa fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa fa-instagram"><span class="label">Instagram</span></a></li>
        <li><a href="#" class="icon fa fa-dribbble"><span class="label">Dribbble</span></a></li>
        <li><a href="#" class="icon fa fa-envelope-o"><span class="label">Email</span></a></li>
      </ul>
      <ul class="copyright">
        <li>&copy; Eat Easy</li>
        <li>Design: Andrea Cardiota, Ruben Ceroni, Luca Giulianini</li>
      </ul>
    </footer>

   <!-- Scripts -->
  <script src="assets/js/Jquery/jquery-1.11.1.min.js"></script>
  <script src="assets/js/Jquery/jquery.backstretch.min.js"></script>
  <script src="assets/js/Jquery/jquery.scrollex.min.js"></script>
  <script src="assets/js/Jquery/jquery.scrolly.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>

  <script src="assets/js/cart.js"></script>
  <script src="assets/js/util.js"></script>
</body>
</html>
