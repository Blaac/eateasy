<?php 

include 'dbConnectAdmin.php';
include 'secure.php';

sec_session_start();
if(login_check($conn) == true) {
	if (!checkAdmin()) {
		header('Location: ./indexLogged.php');
	}

	if (!empty($_GET["productName"])) {
		$name = $_GET["productName"];

		$sql = "SELECT P.Immagine FROM prodotto AS P WHERE P.Nome = ?";
		$stmt = $conn->prepare($sql);
		$stmt->bind_param('s', $name);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($immagine); // recupera le variabili dal risultato ottenuto.


		while ($stmt->fetch()) {
			unlink("$immagine");
		}
		$stmt->free_result();
		$stmt->close();



		$connect = new mysqli(HOST, USER, PASSWORD, DATABASE);
		$delete_stmt = $connect->prepare('DELETE FROM prodottomenu WHERE prodottomenu.Nome = ?');

		$delete_stmt->bind_param('s', $name);

		// Esegui la query ottenuta.
		$delete_stmt->execute();
		$delete_stmt->free_result();
		$delete_stmt->close();
	}

}
?>
