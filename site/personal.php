<?php
	include 'dbConnect.php';
	include 'secure.php';
	include 'persQuery.php';

	$username;

	$nome;
	$cognome;
	$indirizzo;
	$mail;

	sec_session_start();
 	if(login_check($conn) == true) {
     $username = $_SESSION['username'];


		if ($stmt = $conn->prepare("SELECT cliente.nome, cliente.cognome, cliente.indirizzo, cliente.mail FROM cliente WHERE mail = ?")) {
			$stmt->bind_param('s', $username); // esegue il bind del parametro '$user_id'.
			$stmt->execute(); // Esegue la query creata.
			$stmt->store_result();
			$stmt->bind_result($nome, $cognome, $indirizzo, $mail); // recupera le variabili dal risultato ottenuto.
			while ($stmt->fetch()) {
			}
			$stmt->free_result();
	    $stmt->close();
    }
?>


<!DOCTYPE html>
<html lang="it-IT">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo "$nome 's area" ?></title>

  <!-- CSS RESET-->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
  <link rel="stylesheet" type="text/css" href="assets/css/Main/reset.css">
  <!-- CSS IMOPORT-->
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <!-- CSS-->
  <link rel="stylesheet" type="text/css" href="assets/css/Personal/personal.css">
  <link rel="stylesheet" type="text/css" href="assets/css/Carrello/footer-bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/Basics/lateralBar.css">



</head>

<body>
    <header id="header" class="my-header">
      <h1 class="titolo panel-heading"><span class="icon cart-moving fa fa-user"></span> Ciao! <?php echo $nome ?> </h1>
       <nav id="nav">
       <ul>
         <li class="special">
           <a href="#menu" class="menuToggle"><span></span></a>
           <div id="menu">
             <ul>
               <li><a href="index.php">Home</a></li>
               <li><a href="personal.php">Area Personale</a></li>
               <li><a href="menu.php">Menù</a></li>
               <li><a href="carrello.php">Carrello</a></li>
               <?php if(checkAdmin()) {
               echo "<li><a href='admin.php'>Admin</a></li>";
               } ?>
               <li><a href="logout.php">Logout</a></li>
             </ul>
           </div>
         </li>
       </ul>
     </nav>
    </header>

<main>

    <section class="col-sm-5 container-fluid">
    	<div class="pannello-user">
	        <h2>User <span class="fa fa-table icon"></span></h2>
	        	<ul class="user-list">
	        		<li>Nome:  <?php echo $nome ?>     </li>
	        		<li>Cognome:   <?php echo $cognome ?>     </li>
	        		<li>Indirizzo:   <?php echo $indirizzo ?>     </li>
	        		<li>Mail:   <?php echo $mail ?>     </li>
	        	</ul>

    	</div>
    </section>


    <section class="col-sm-7 container-fluid">
    	<div class="pannello-user">
	        <h2>Ordini <span class="fa fa-table icon"></span></h2>
          <div class="table-responsive">
        	 <table class="table">
        		<tr>
              <th>#</th>
              <th>Mail</th>
              <th>Data</th>
              <th>Indirizzo</th>
              <th>Prodotti</th>
              <th>Prezzo</th>
              <th>Spedito</th>
        		</tr>
        		<?php
        			orders();
        		 ?>
        	</table>
        </div>
    	</div>
    </section>



  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content pannello-user pannello-modal">
          <h3 class="modal-title">Ordine <span class="numero-ordine-modal"></span> <span class="fa fa-table icon"></span></h3>
          <div class="table-responsive">
            <table class="table">
              <tr class="dettaglio-ordine">
                <th>Nome</th>
                <th>Categoria</th>
                <th>Quantità</th>
                <th>Prezzo</th>
              </tr>
            </table>
          </div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>


</main>



  <!-- Footer -->
    <footer class="col-sm-12 footer">
      <ul class="icons">
        <li><a href="#" class="icon fa fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa fa-instagram"><span class="label">Instagram</span></a></li>
        <li><a href="#" class="icon fa fa-dribbble"><span class="label">Dribbble</span></a></li>
        <li><a href="#" class="icon fa fa-envelope-o"><span class="label">Email</span></a></li>
      </ul>
      <ul class="copyright">
        <li>&copy; Eat Easy</li>
        <li>Design: Andrea Cardiota, Ruben Ceroni, Luca Giulianini</li>
      </ul>
    </footer>

   <!-- Scripts -->
  <script src="assets/js/Jquery/jquery-1.11.1.min.js"></script>
  <script src="assets/js/Jquery/jquery.backstretch.min.js"></script>
  <script src="assets/js/Jquery/jquery.scrollex.min.js"></script>
  <script src="assets/js/Jquery/jquery.scrolly.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>

  <script src="assets/js/personal.js"></script>
  <script src="assets/js/util.js"></script>

</body>
</html>

<?php
} else {
   echo 'You are not authorized to access this page, please login. <br/>';
	 header('Location: index.php');
 }
 ?>
