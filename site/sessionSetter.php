<?php
include 'dbConnect.php';
include 'secure.php';

sec_session_start();
if(login_check($conn) == true) {
    if (isset($_POST['ordine'])) {
      $_SESSION['prodotti'] = $_POST['ordine'];

    }
    
    if (isset($_POST['notification'])) {
    	if (isset($_SESSION['signed'])) {
    		$_SESSION['signed'] = null;
        echo "registrato";
        exit;
    	}
      if (isset($_SESSION['logged']) && !isset($_POST['notification'])) {   // LOGGATO
        $_SESSION['logged'] = null; 
        echo "loggato";
        exit;
    	}
      if (isset($_SESSION['acquistato'])) {   // ORDINE CONFERMATO DALL'UTENTE
        $_SESSION['acquistato'] = null;
        echo "pagato";
        exit;
      }
    }
}
?>
